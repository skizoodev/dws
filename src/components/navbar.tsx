'use client'
import { useEffect, useState } from "react"
export default function Navbar() {
    const  [check,setCheck]=useState(false)
    const [subMenu,setSub]=useState("hs-collapse hidden overflow-hidden transition-all duration-300 basis-full grow sm:block")
    const [navbarBg, setNavbarBg] = useState('bg-transparent w-screen lg:w-full fixed top-[46px] left-0 pb-1 z-50');
    useEffect(()=>{
        if(check){
setSub("hs-collapse overflow-hidden transition-all duration-300 basis-full grow sm:block")
        }
        else{
            setSub("hs-collapse hidden overflow-hidden transition-all duration-300 basis-full grow sm:block")
        }
    },[check])
    const handleScroll = () => {
      if (window.scrollY > 100) {
        setNavbarBg('flex flex-wrap z-40 fixed top-[0px] sm:justify-start sm:flex-col z-50 w-full bg-white border-b border-gray-200 text-sm pb-2 sm:pb-0'); // Add your desired background color class
      } else {
        setNavbarBg('flex z-40 flex-wrap sm:justify-start sm:flex-col z-50 w-full bg-transparent border-b border-gray-200 text-sm pb-2 sm:pb-0');
      }
    };
  
    useEffect(() => {
      window.addEventListener('scroll', handleScroll);
      return () => {
        window.removeEventListener('scroll', handleScroll);
      };
    }, [navbarBg]);
  
 return (
<header className={navbarBg}>
  {/* Topbar */}
  <div className="max-w-7xl mx-auto w-full px-4 sm:px-6 lg:px-8">
    <div className="flex items-center justify-end gap-x-5 w-full py-2 sm:pt-2 sm:pb-0">
      <a
        className="inline-flex justify-center items-center gap-2 font-medium text-slate-600 hover:text-slate-500 text-sm "
        href="#"
      >
        <svg
          className="w-3 h-3"
          xmlns="http://www.w3.org/2000/svg"
          width={16}
          height={16}
          fill="currentColor"
          viewBox="0 0 16 16"
        >
          <path d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm7.5-6.923c-.67.204-1.335.82-1.887 1.855A7.97 7.97 0 0 0 5.145 4H7.5V1.077zM4.09 4a9.267 9.267 0 0 1 .64-1.539 6.7 6.7 0 0 1 .597-.933A7.025 7.025 0 0 0 2.255 4H4.09zm-.582 3.5c.03-.877.138-1.718.312-2.5H1.674a6.958 6.958 0 0 0-.656 2.5h2.49zM4.847 5a12.5 12.5 0 0 0-.338 2.5H7.5V5H4.847zM8.5 5v2.5h2.99a12.495 12.495 0 0 0-.337-2.5H8.5zM4.51 8.5a12.5 12.5 0 0 0 .337 2.5H7.5V8.5H4.51zm3.99 0V11h2.653c.187-.765.306-1.608.338-2.5H8.5zM5.145 12c.138.386.295.744.468 1.068.552 1.035 1.218 1.65 1.887 1.855V12H5.145zm.182 2.472a6.696 6.696 0 0 1-.597-.933A9.268 9.268 0 0 1 4.09 12H2.255a7.024 7.024 0 0 0 3.072 2.472zM3.82 11a13.652 13.652 0 0 1-.312-2.5h-2.49c.062.89.291 1.733.656 2.5H3.82zm6.853 3.472A7.024 7.024 0 0 0 13.745 12H11.91a9.27 9.27 0 0 1-.64 1.539 6.688 6.688 0 0 1-.597.933zM8.5 12v2.923c.67-.204 1.335-.82 1.887-1.855.173-.324.33-.682.468-1.068H8.5zm3.68-1h2.146c.365-.767.594-1.61.656-2.5h-2.49a13.65 13.65 0 0 1-.312 2.5zm2.802-3.5a6.959 6.959 0 0 0-.656-2.5H12.18c.174.782.282 1.623.312 2.5h2.49zM11.27 2.461c.247.464.462.98.64 1.539h1.835a7.024 7.024 0 0 0-3.072-2.472c.218.284.418.598.597.933zM10.855 4a7.966 7.966 0 0 0-.468-1.068C9.835 1.897 9.17 1.282 8.5 1.077V4h2.355z" />
        </svg>
        English (US)
      </a>
      <a
        className="inline-flex justify-center items-center gap-2 font-medium text-slate-600 hover:text-slate-500 text-sm "
        href="#"
      >
        Sign In
      </a>
      <a
        className="py-2 px-3 inline-flex justify-center items-center gap-2 rounded-md border-2 border-gray-200 font-semibold text-blue-600 hover:bg-blue-50 hover:border-blue-100 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 transition-all text-sm"
        href="#"
      >
        Get started
      </a>
    </div>
  </div>
  {/* End Topbar */}
  <nav
    className="relative max-w-7xl w-full mx-auto px-4 sm:flex sm:items-center sm:justify-between sm:px-6 lg:px-8"
    aria-label="Global"
  >
    <div className="flex items-center justify-between">
      <a
        className="flex-none text-xl font-semibold "
        href="#"
        aria-label="Brand"
      >
        LOGO
      </a>
      <div className="sm:hidden">
        <button
          type="button"
          onClick={()=>setCheck(!check)}
          className="hs-collapse-toggle p-2 inline-flex justify-center items-center gap-2 rounded-md border font-medium bg-white text-gray-700 shadow-sm align-middle hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-white focus:ring-blue-600 transition-all text-sm "
          data-hs-collapse="#navbar-collapse-with-animation"
          aria-controls="navbar-collapse-with-animation"
          aria-label="Toggle navigation"
        >
          <svg
            className="hs-collapse-open:hidden w-4 h-4"
            width={16}
            height={16}
            fill="currentColor"
            viewBox="0 0 16 16"
          >
            <path
              fillRule="evenodd"
              d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"
            />
          </svg>
          <svg
            className="hs-collapse-open:block hidden w-4 h-4"
            width={16}
            height={16}
            fill="currentColor"
            viewBox="0 0 16 16"
          >
            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
          </svg>
        </button>
      </div>
    </div>
    <div
      id="navbar-collapse-with-animation"
      className={subMenu}
    >
      <div className="flex flex-col gap-y-4 gap-x-0 mt-5 sm:flex-row sm:items-center sm:justify-end sm:gap-y-0 sm:gap-x-7 sm:mt-0 sm:pl-7">
        <a
          className="font-medium sm:py-6 text-blue-600"
          href="#"
          aria-current="page"
        >
          Landing
        </a>
        <a
          className="font-medium text-gray-800 hover:text-gray-500 sm:py-6 "
          href="#"
        >
          Account
        </a>
        <a
          className="font-medium text-gray-800 hover:text-gray-500 sm:py-6 "
          href="#"
        >
          Work
        </a>
        <a
          className="font-medium text-gray-800 hover:text-gray-500 sm:py-6 "
          href="#"
        >
          Blog
        </a>
        
      </div>
    </div>
  </nav>
</header>
  )
}
